---
id: doc1.2
title: Appropriate Development Approach for this Project. 
sidebar_label: SDA For my Project
---
## Choosing the right approach for me
While coming up with the initial idea for how I would complete this project, I brainstormed many times and researched about the many different SDA's that are commonly used while developing any form
of software. While that step was important, the most important part was being able to narrow down my search to the  approaches that would help me build not just any type of software, but a
**web app**

As I looked for the optimal approach to complete an efficient solution which most likely will not be updated in the future, I stumbled upon an approach that I had previously studied, which was the Rapid application development approach, otherwise known as RAD. 

The RAD approach is defined as:
>Rapid application development (RAD) describes a method of software development which heavily emphasizes rapid prototyping and iterative delivery. The RAD model is, therefore, a sharp alternative to the typical waterfall development model, which often focuses largely on planning and sequential design practices.

Heres a graphical representation of the RAD Approach:
![alt text](https://www.wavemaker.com/wp-content/uploads/RAD-Methodology.png "RAD Approach")

This approach seemed ideal for this project for 3 main reasons:

1. I had already worked with the RAD Approach before.
2. I needed to make sure the final solution was as bug free and error free as it could be.
3. The RAD Approach is easier to carry out as an individual.

Making sure I had chosen the right approach for this project was a vital part of completing with time to spare for debugging and fixing errors, as I wanted this program to be perfect. 

## Comparison to the Previous Case Study
Previously I looked at Facebook, more specifically the early development days of Facebook and how they implemented such a genius idea in a relatively short timespan. Using aspects such as server maintenance and usability, I was able to ascertain the fact that during the ongoing development of Facebook, an iterative approach is definitely the most effective way to continually improve a product with such a high demand. 

Comparing facebooks iterative approach to my implementation of the RAD approach seems like a hard task, however, analysing the fundamental aspects of both of these approaches, their pros and cons, and how they apply to their related software is a good method of comparison

|                                             Facebooks Iterative Approach                                            |                                                       My RAD Approach                                                       |
|:-------------------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------:|
| Initial project is quite underdeveloped. However many improvements are made over the years.                         | Finalised solution is presented through A cycle of prototypes and testing                                                   |
| Constantly rolling out new versions and are unable to ever complete the  product                                    | First implementation of the project is usually the last, which may lead to  unforeseen issues                               |
| More room for implementation of customer feedback, which may lead to a happier userbase                             | Many prototypes are developed in a short time span which helps identify and squash any bugs present in the earlier versions |
| Due to the cost of running such large servers overtime, ads have had to be  implemented as a main part of facebook. | Although the code may work perfectly fine, it may seem untidy or hard to read from an outside perspective.                  |

After analysing some similarities and difference between the two approaches, I have come to the conclusion that there is no neccesarily *"right"* approach, rather, an approach that is more suited towards the type of project that is being completed. 