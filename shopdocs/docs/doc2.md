---
id: doc2
title: Screen Designs for my Final Solution
sidebar_label: Screen Design
---
## Early Screen Designs
Designing a user friendly and pleasing U.I was an important aspect when starting off with this project. I created 3 rough photoshop sketches to try and plan out how my ui would look.

## Store Page
This very rough sketch was the first ever drawn up iteration of the final product that can be accessed today. It has space or multiple items, prices, text and more fields. The nav bar is concise and has plenty of functionality implemented in it.

![alt_text](https://i.ibb.co/y6LC48k/Screen-Shot-2020-05-08-at-9-27-47-pm.png)

## Cart Page
The cart page was essential to get right, as I needed to display all the proper information on the page in order for the user to fully understand what items that they were buying. Spreading elements out aided in that process of making the U.I more readable and accessible

![alt_text](https://i.ibb.co/KcnFbxX/Screen-Shot-2020-05-08-at-9-36-44-pm.png)

## Checkout Page
The final page of the website has a lot of functionality hidden under a small amount of front end elements. The order summary on the right hand side was in my mind since the beginning of the project, but I was unsure of how I was going to use the space on the left. Eventually, I came up with the details idea that is implemented now. 

![alt_text](https://i.ibb.co/CmTNczp/Screen-Shot-2020-05-08-at-9-52-37-pm.png)