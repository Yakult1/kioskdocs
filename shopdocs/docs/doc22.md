---
id: doc2.2
title: Summarising the Development Experience
sidebar_label: Conclusion
---
## My Experience
Throughout the duration of this project, I have learnt an insane amount about web development with a python backend, js front end and other aspects about creating a python webapp. The process of creating detailed documentation proved to be challenging  and it was overall an amazing experience.

> Thank you to the people who aided me during the development process, your efforts were much appreciated.