---
id: doc1.1
title: Case study of Software Development Approaches
sidebar_label: Case Study
---
## Introduction to SDA's
In software engineering, a software development approach is the process of dividing software development work into distinct phases to improve design, product management, and project management. It is also known as a software development life cycle (SDLC). The methodology may include the pre-definition of specific deliverables and artifacts that are created and completed by a project team to develop or maintain an application.

Since there are many approaches to developing software, for this case study I would like to focus on the 4 most popular approaches to developing software (according to wikipedia).
These are:

1. **Waterfall**
2. **Agile**
3. **Iterative**
4. **Rapid Application Development**

All these different approaches come with their own pro's and con's, and allow for the development of many different kinds of software.

## SDA's in Real Life
For this case study, I would like to take a look at facebook as an example of the perfect use of an SDA. Facebook was created in a college dorm room in 2004, and now, over 2.2 Billion people in the
World are signed up with A Facebook account. Why is this the case?

Well for starters, looking back at the development era of facebook, although we dont know for sure whether or not Mark Zuckerberg used used any certain approach, it is quite evident that a version of the iterative approach was used. The definition of an iterative approach in software development is:

>The iterative model is a particular implementation of a software development life cycle (SDLC) that focuses on an initial, simplified implementation, which then progressively gains more complexity and a broader feature set until the final system is complete.

Heres a graphical representation of the iterative approach:
![alt text](https://www.professionalqa.com/assets/images/iterative-model.png "RAD Approach")

Although facebook has and probably will not see a *"final system"*, the iterative model is a good way to describe the constant updates and bugfixes which progressively have proved to be more challenging as the userbase for facebook exponentially rises. Tasks such as maintaining and running servers for Facebook to run on, Building user friendly U.I's and making Facebook easily accessible all have become more difficult over the lifespan of Facebook. 


