---
id: doc1
title: Specifications for this Project
sidebar_label: Specifications
---
## Understanding the problem
To be able to effectively undertake this task, I needed to be able to understand the problem at hand. 
We were given the problem statement, which reads: 

>In the last decade, self-serve shopping kiosks are an emerging technology that seek to improve the customer experience when shopping. Whilst there have been vast improvements over the years, with many versions of the interface developed by different approaches, users are still not entirely happy. There are also many technical needs for the self-serve kiosk that are not entirely working well, including stock updates after purchase, items not scanning correctly, and the need for a customer service assistant to help customers with technical issues.

Given this statement, our task was to:

>Design, plan and implement a prototype of a kiosk which addresses the needs of the customer as well as the needs of the supermarket system.

Now that I knew what my task was, I needed to figure out how I would approach such a task.

## Resources used in Completing the Task

I used many frameworks and programming languages during the completion of this task, so it would be helpful to compile a table of all the resources I have used over the duration of this project.

| Languages/Markdowns| Frameworks    |       Other        |
|:------------------:|:-------------:|:------------------:|
|  Python            | Django        | Visual Studio Code |
|  Javascript        | React.js      | Stack Overflow     |
|  json              |               | GitHub             |
|  Markdown          |               | GitLab             |  
|  CSS               |               | Google             |   
|  HTML5             |               | Youtube            |
|  Bash              |               | Python Anywhere    |

Not mentioned in the table, There are many skills and aspects of programming that I used while creating the end product. Things such as data dictionaries, implementation of data structures, Screen designs, Storyboards and finally the Coded solution (my source code). Combining all of these skills and aspects of programming together has allowed me to solve the problem in an efficient and easy to use way.

## Summary
To summarise, I have specifically used a wide range of Programming Languages, Markdown languages, Web development Frameworks and a mixture of different Software to assist me 
in the development of the final product.