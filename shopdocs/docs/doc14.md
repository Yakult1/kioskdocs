---
id: doc1.4
title: Data representation in the form of a Data Dictionary
sidebar_label: Data Dictionary
---
## Data Dictionary
This data dictionary aims to include the various types of data structures and types that are included throughout the whole development of the final product. 

| Name           | Data Type                     | Description                                                                                                                       | Size              |
|----------------|-------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|-------------------|
| CartData       | String, Floating Point,  PNG  | Stores the prices,  total prices, amounts, total amounts, names and images of the items in the cart                               | Subject to change |
| product.id     | String                        | The specific string assigned to  separate products in the list of products                                                        | 16 Characters     |
| transaction.id | String                        | String produced after the condition "transaction completed" is satisfied                                                          | 8 Characters      |
| Order.shipping | Integer, String               | A list containing the data produced by the users input into the Shipping Details field                                            | Subject to Change |
| StoreData      | String, Floating Point, PNG   | Stores the prices, images and names of all the items that are available on the story.                                             | Subject to Change |
| UrlPatterns    | Strings, Variables            | The back end list responsible for  redirecting users when they interact with front end features such as  buttons.                 | Subject to Change |
| guestOrder     | Strings, variables,  integers | A list responsible for handling the form submission when a user enters their details into the "details" fields.                   | Subject to Change |
| cookieData     | Strings                       | Stores the cookies produced by a user if they choose to save their details with the website                                       | Subject to Change |
| StoreConfig    | Strings, variables,  Integers | back end script that communicates with  front end css to change properties such as page title etc.                                 | Subject to Change |
| DATABASES      | Strings, Variables,  Integers | A dictionary that contains configuration scripts for the file 'db.sqlite3', which helps organise databases on the back end side.  | Subject to Change |