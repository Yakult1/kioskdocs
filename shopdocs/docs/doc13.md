---
id: doc1.3
title: IPO Chart for my Project
sidebar_label: IPO Chart
---
## What is an IPO Chart?
An input process output, or IPO, chart is simply a way to describe how your program processes information. Usually, an IPO chart is the precursor to using software for specific purposes. The chart has three components, and you write the description of each component in plain English, not code or mathematical formulas. To make an IPO chart, you need to complete 4 main steps:

1. **Read and understand the problem**
2. **Identify the outputs. Pick intrinsic variable names that represent the output**
3. **Decide what data (the inputs) is required in order to get the required output Sometimes these will be given to you, sometimes not, which means you may need to invent inputs.**  
4. **Once you have the outputs and the inputs you need to determine how the inputs can be transformed into the required outputs. This is a dynamic process which may mean you need to continually return to the previous steps.**

These are the essential aspects to understanding how an IPO chart is made, now lets put these skills into practice. What you see below is a complete IPO Chart which represents all the possible inputs, outputs and processes that happen during a typical shopping session using the kiosk. There are 4 main HTML pages that the user must cycle through if they wish to successfully place an order. An IPO chart will be constructed for each page in order to fully grasp every aspect of the program.

## Main.html
The Main page sets up the foundations for the webapp, doing things such as linking css and controlling the navbar. The user is only able to interact with the Main.html Page by pressing one of the 4 buttons on the nav bar.

|                                    Input                                    |                                     Process                                    |                                                              Output                                                             |
|:---------------------------------------------------------------------------:|:------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------------------:|
| User clicks on the Text <br>that says "Self Serve Kiosk"<br>in the nav bar. |                     The program links to the<br>store page.                    |                                       The store page is displayed<br>on the users screen,                                       |
|    User clicks on the Text <br>that says "Groceries"<br>in the nav bar.    |                     The program links to the<br>store page.                    |                                       The store page is displayed<br>on the users screen,                                       |
|  User clicks on the Button <br>that says "Documentation"<br>in the nav bar. | The program links to an<br>external site which is<br>hosting the documentation |                                Documentation landing page<br>is displayed on the users<br>screen.                               |
|               User clicks on the "cart" icon<br>in the nav bar              |                   The program links to the<br>cart.html page.                  | The cart page is displayed,<br>showing total items in cart,<br>total price, a checkout button<br>and a back to shopping button. |

## Store.html
The Store page is what the user first sees when they open the website. It shows the user the available products, the prices of those products, an add to cart button, and it sets the dimensions for the product element. Interacting with the store page is a fundamental aspect of the program, and is quite succinct and easy to do.

|                                       Input                                       |                                     Process                                     |                                                                   Output                                                                   |
|:---------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------:|
| User clicks on the "Add to cart" button which is under the name of every product. | The program parses the product.id of the chosen product to the cart.html page, which stores the product in the cart.  | The number above the cart icon which represents the amount of items in the cart will increment by 1, and will be  displayed on the screen. |

## Cart.html
The cart page provides the user a way to finalise their product choice, and allows for the addition or removal of amounts of product that are already in the cart. The cart page allows the user to interact with their personalised cart efficiently and easily. 

|                                  Input                                  |                                                                                                Process                                                                                                |                                                                                                          Output                                                                                                          |
|:-----------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| User clicks the up or down arrow to the right of a product in the cart. | The product.id of that specific product will increment by 1 while the price will be multiplied by the amount of said product in the cart. The total price and total items will be updated accordingly | The cart page will display the  updated quantity of said product,  while also displaying the updated total price of  said product in cart. The final cost and amount of  items are displayed easily for the user to see. |
|                 User clicks the Continue shopping button                |                                               The program will link the user back to the store page while still keeping the current items in the cart.                                                |                                                          The store page is displayed once again, allowing the user to add new items to their personalised cart.                                                          |
| User clicks the Checkout Button                                         | The program sends the information in the cart (quantity of each product, total amount of item and total price) to the  checkout page, and stores it in the order summary data structure.              |                                The "checkout" page is displayed,  which contains an order summary, along with allowing the user to fill in their  information and finalise their purchase.                               |

## Checkout.html
The final page that is displayed on the website is the checkout page, which allows the user to see a summary of their order, including all the relevant information regarding their order. The user is required to enter their name and email for a virtual receipt. As this is a self serve kiosk, the user will most likely be accessing this at some sort of supermarket, but in case they are shopping online, they are able to input shipping information. 

|                                       Input                                      |                                                                 Process                                                                |                                                                              Output                                                                             |
|:--------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| User inputs their personal details and shipping information and presses continue |             The users information is stored in an encrypted database, and can be reused easily by saving the information.              |                     The website prompts the user if they want to save the following profile. The "make payment" button now becomes available                    |
|                      User clicks the "make payment"  button                      |  The program sends a csrf token to the website validating the purchase. Once that purchase is validated, the transaction is complete.  | The user is prompted with a message that says "Transaction Complete". Once they press okay, they are redirected back to the store page and their cart is reset. |
|                         User Clicks "Back to Cart" button                        | The program keeps all of the items stored in the cart, and re-directs the user back to the cart.html page for additional customisation |                                                          The carts.html page is displayed to the user.                                                          |



