---
id: doc1.5
title: Central Algorithms of my Final Solution
sidebar_label: Algorithms
---
## Representing Control and Data structures
For my project, I have used various control and data structures which need to be represented in the form of a clear and concise algorithm. To do this, I will be creating flowcharts and pseudocode that visualise the backend Python development of the 3 main pages.

## Flow Charts
### Store
Although simple, the flowchart representing the process of going through the store page is easy to understand and perfectly displays the use of the list data structure in the form of product.id

![alt_text](https://i.ibb.co/mF7zVzC/store-1.png)


### Cart
The flowchart representing the cart page accurately depicts a simple way that users are able to alter their cart contents while on the cart page. Once they have clicked checkout, they will get sent to the final page, which is the Checkout page

![alt_text](https://i.ibb.co/1d4Tz43/cart-1.png)


### Checkout
The final page of the program provides the user with a different payment solution depending on the mode of shopping. If the user is using this kiosk in person, there is no need for the use of shipping information. However, the option is there for online users. After making the payment, the cycle will restart.

![alt_text](https://i.ibb.co/x5Lp8LR/Checkout.png)


## Pseudocode
### Store
Writing out the corresponding flowchart for the store page in pseudocode helps us understand how the page's logic works. The python backend code is obviously more complicated and difficult, but psuedocode does a great job of outlining the logic involved in this page.

```
BEGIN store
    DISPLAY PRODUCTS
    USER clicks "Add to Cart"
    __store.html__ send product.id to __cart.html__
    DISPLAY updated cart amount
END store
```

### Cart
The cart page utilises python lists (arrays) to effectively update the right information based on the users request. Checking out with the items in the cart takes the user to the final page of the program.

```
BEGIN cart
    IF USER selects "increase quantity arrow":
        INCREASE product.amount by 1
        DISPLAY product.amount, price.total
        USER selects "checkout"
    ELSE:
        USER selects "checkout"
    ENDIF
END
```

### Checkout
For the final page, storing various data types in multiple data structures with python is the most effective way to guarantee a smooth end to the shopping cycle. The "return" subprogram simply removes all items from the cart, and sends the user back to the store page.

```
BEGIN
    USER INPUT details
    DISPLAY "Do you wish to save these details?"
    IF USER selects "Yes":
        STORE details IN cookies
        DISPLAY "Make Payment"
        USER selects "Make Payment"
        __return__
    ELSE:
        DISPLAY "Make Payment"
        USER selects "Make Payment"
        __return__
    ENDIF
END
```


