---
id: doc3
title: Credits
sidebar_label: Important Contributors
---
## Logo 
The Kiosk Logo was drawn by my girlfriend, you can find her other art [@kyufait](https://www.instagram.com/kyufait/?hl=en)

## Art
The art on this page is provided by [undraw illustrations](https://undraw.co/illustrations).